//
//  AppDelegate.h
//
//  Created by Tim Perfitt on 7/15/13.
//  Copyright (c) 2014 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>



@interface AppDelegate : NSObject <NSApplicationDelegate,NSUserNotificationCenterDelegate> {
        NSString *password;
}
@property (weak) IBOutlet NSMenu *statusMenu;

@property (nonatomic,strong) NSStatusItem *theItem;
@property (nonatomic,strong) NSStatusBar *bar;

@end
