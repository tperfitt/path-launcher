//
//  AppDelegate.m
//  macgeohopper
//
//  Created by Tim Perfitt on 7/15/13.
//  Copyright (c) 2014 Twocanoes Software. All rights reserved.
//

#import "AppDelegate.h"
#import "TCSLauncherProvider.h"
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <assert.h>

NSString *RealHomeDirectory2() {
    struct passwd *pw = getpwuid(getuid());
    assert(pw);
    return [NSString stringWithUTF8String:pw->pw_dir];
}

@interface AppDelegate()
@property (retain, nonatomic) TCSLauncherProvider* launcherProvider;
@property (retain, nonatomic) NSURL *url;
@end

@implementation AppDelegate


- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSAppleEventManager sharedAppleEventManager] setEventHandler:self andSelector:@selector(handleURLEvent:withReplyEvent:) forEventClass:kInternetEventClass andEventID:kAEGetURL];

    }
    return self;
}


#pragma mark IBActions

- (void)handleURLEvent:(NSAppleEventDescriptor*)event withReplyEvent:(NSAppleEventDescriptor*)replyEvent
{
    NSString* url = [[event paramDescriptorForKeyword:keyDirectObject] stringValue];
    if (![url hasPrefix:@"path://"]) return;
    
  
    url=[url stringByRemovingPercentEncoding];
    NSString *homeDir=RealHomeDirectory2();
    
    url = [url substringFromIndex:[@"path://" length]];


    NSString *normalized=[url stringByReplacingOccurrencesOfString:@"~" withString:homeDir];
    self.url=[[NSURL alloc]  initFileURLWithPath:normalized];
    
    
    [[NSWorkspace sharedWorkspace] activateFileViewerSelectingURLs:@[self.url]];
    [NSApp terminate:self];
    
    
    
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification{

    self.launcherProvider=[[TCSLauncherProvider alloc] init];
    [NSApp setServicesProvider:self.launcherProvider];

}


@end
