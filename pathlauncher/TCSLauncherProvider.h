//
//  TCSLauncherProvider.h
//  PathLauncher
//
//  Created by Tim Perfitt on 5/18/14.
//  Copyright (c) 2014 Twocanoes Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCSLauncherProvider : NSObject
- (void)exportData:(NSPasteboard *)pasteboard userData:(NSString *)data error:(NSString **)error;
@end
