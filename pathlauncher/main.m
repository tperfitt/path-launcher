//
//  main.m
//  macgeohopper
//
//  Created by Tim Perfitt on 7/15/13.
//  Copyright (c) 2014 Twocanoes Software. All rights reserved.
//

#import <Cocoa/Cocoa.h>
int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
