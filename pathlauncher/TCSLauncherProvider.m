//
//  TCSLauncherProvider.m
//  PathLauncher
//
//  Created by Tim Perfitt on 5/18/14.
//  Copyright (c) 2014 Twocanoes Software. All rights reserved.
//

#import "TCSLauncherProvider.h"
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <assert.h>

NSString *RealHomeDirectory() {
    struct passwd *pw = getpwuid(getuid());
    assert(pw);
    return [NSString stringWithUTF8String:pw->pw_dir];
}
@implementation TCSLauncherProvider
-(NSArray *)pasteboardArrayWithPasteboard:(NSPasteboard *)inPasteboard{
    return [inPasteboard
     readObjectsForClasses:[NSArray arrayWithObject:[NSURL class]]
     options:[NSDictionary dictionaryWithObject:[NSNumber
                                                 numberWithBool:YES] forKey: NSPasteboardURLReadingFileURLsOnlyKey]];

}
-(void)postNotificationMessage{

    NSUserNotification *notification = [[NSUserNotification alloc] init];
    //Set the title of the notification
    [notification setTitle:@"Path Launcher"];
    //Set the text of the notification
    [notification setInformativeText:@"Path copied to pasteboard."];
    //Set the time and date on which the nofication will be deliverd (for example 20 secons later than the current date and time)
    notification.hasActionButton=NO;
    //Get the default notification center
    NSUserNotificationCenter *center = [NSUserNotificationCenter defaultUserNotificationCenter];
    [center deliverNotification:notification];

    [center performSelector:@selector(removeDeliveredNotification:) withObject:notification afterDelay:2];

    [NSApp performSelector:@selector(terminate:) withObject:self afterDelay:3];

}
- (void)exportData:(NSPasteboard *)pasteboard userData:(NSString *)data error:(NSString **)error {
    NSArray *ar=[self pasteboardArrayWithPasteboard:pasteboard];
    if (!ar || [ar count]==0) {
        [NSApp  terminate:self];

        return;
    }
    NSURL *url=[[ar objectAtIndex:0] filePathURL];
    
    NSString *workingString;
    NSString *realHome=RealHomeDirectory();
    if ([[url path] hasPrefix:realHome]) {
       
        workingString=[[url path] stringByReplacingOccurrencesOfString:realHome withString:@""] ;
        
        workingString=[@"~" stringByAppendingString:workingString];
    }
    else {

        workingString=[url path];
    }

    NSString *finalString=[@"path://" stringByAppendingString:[[[[[workingString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByReplacingOccurrencesOfString:@"~" withString:@"%7e"] stringByReplacingOccurrencesOfString:@"/" withString:@"%2f"] stringByReplacingOccurrencesOfString:@"(" withString:@"%28"] stringByReplacingOccurrencesOfString:@")" withString:@"%29"]];
    
    [[NSPasteboard generalPasteboard] clearContents];
    [[NSPasteboard generalPasteboard] writeObjects:[NSArray arrayWithObject:finalString]];

    [self postNotificationMessage];
    
}
//

@end
