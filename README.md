A simple utility for OS X to solve the problem of sharing local links with other folks and have them clickable.  For example, we share a lot of files that have paths like /Users/tperfitt/Dropbox/Twocanoes Software/Images/graphic.png.  If you share the local path using the dropbox contextual menu, you get a URL to the dropbox website that doesn't tell you where the file is locally.  

With this utility installed, you simply select a file or folder, open the contextual menu (or hit command-=), and then paste the URL.  The url will look like path://~/Dropbox/Twocanoes%20Software/Images/graphic.png, and you can share it via email or any other way. When the person receives it and have Path Launcher installed, they can click the link and the finder will reveal the file or folder locally.

If the path is located in the user's home folder, it will resolve the local home directory path to ~. If it is outside the home directory, it will just be the path.

Thanks to http://twitter.com/mikeymikey and http://twitter.com/pmbuko for helping work out the details, to http://twitter.com/pmbuko for his excellent blog post and to http://twitter.com/mikeymikey for his python service that motivated me to write it in objective-c.

### How do I get set up? ###

Build and install Path Launcher in your applications folder.  Or install the prebuilt package from https://bitbucket.org/tperfitt/path-launcher/downloads.  Once installed, OS X will have a new contextual menu in the finder for creating Path Launcher URLs.  OS X will also recognize the path:// URI and reveal items in the finder when clicking links that start with that URI.